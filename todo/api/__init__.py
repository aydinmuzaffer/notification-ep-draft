from flask import Blueprint
from flask_restx import Api

from todo.api.namespaces import todo_ns

API_VERSION = "v1"
blueprint = Blueprint('documented_api', __name__, url_prefix=f'/api/{API_VERSION}')

def register_api(app):
    app.config["RESTX_ERROR_404_HELP"] = False
    app.register_blueprint(blueprint)


api = Api(
    blueprint,
    version=API_VERSION,
    title="Todo API",
    description="A simple todo API.",
)

api.add_namespace(todo_ns)
