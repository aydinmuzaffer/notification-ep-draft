from typing import Literal, Optional, Union, TypedDict
from uuid import UUID
import enum

from todo.services.translation_connector import Languages, TranslationConnector, NoTranslationKeyError 
from todo.services.user import UserBlockedError, UserNotExistError, UserRemovedError, UserService
from todo.services.firebase_connector import FirebaseConnector, FCMTokenExpired, FirebaseError, PayloadParams, generate_payload

class NotificationServiceError(Exception):
    pass

class OrderStatus(str, enum.Enum):
    CREATED = "CREATED"
    CONFIRMED = "CONFIRMED"
    READY_FOR_PICKUP = "READY_FOR_PICKUP"
    CANCELLED = "CANCELLED"
    EXPIRED = "EXPIRED"
    COLLECTED = "COLLECTED"

    def __str__(self):
        return self.value

    def get_event_type(self):
        return f"{self.__class__.__name__}.{self.value}"

class GroupStatus(str, enum.Enum):
    CREATED = "CREATED"
    COMPLETED = "COMPLETED"
    EXPIRED = "EXPIRED"

    def __str__(self):
        return self.value

class NotificationService:
    @staticmethod
    def send_push(
        *,
        user_id: UUID,
        key: Union[
            OrderStatus,
            GroupStatus,
            Literal[
                "reminder",
                "created",
                "community_leader",
                "upcoming_orders",
                "delivery_date_update",
                "ready_for_pickup_reminder",
                "removed_items",
            ],
        ],
        domain: Literal["group", "order", "internal", "pickup_location", "user"],
        title_kwargs: Optional[dict] = None,
        message_kwargs: Optional[dict] = None,
        action: Optional[str] = None,
        order_id: Optional[str] = None,
        payload_params: Optional[PayloadParams] = None,
    ) -> None:

        try:
            user = UserService.get_user_by_id(user_id=user_id, raise_on_blocked=True, raise_on_removed=True, load_related=True)
            user.fcm_tokens.append("token1")
            user.fcm_tokens.append("token2")
            fcm_tokens = user.fcm_tokens
            language = Languages.EN
            translation_connector = TranslationConnector(url="TRANSLATIONS_URL", timeout=10)

            title = translation_connector.get_translation(language=language, key=PUSH_NOTIFICATIONS[domain][key]["title"])
            message = translation_connector.get_translation(language=language, key=PUSH_NOTIFICATIONS[domain][key]["message"])
           
            formatted_title = title.format(**title_kwargs) if title_kwargs else title
            formatted_message = message.format(**message_kwargs) if message_kwargs else message
        except (UserNotExistError, UserBlockedError, UserRemovedError) as e:
            raise e
        except (NoTranslationKeyError, KeyError) as e:
            raise e
        else:
            firebase_connector = FirebaseConnector(
                                                    12,
                                                    "GOOGLE_APPLICATION_CREDENTIALS",
                                                    "FIREBASE_API_KEY",
                                                    "FIREBASE_SHORT_URL_DOMAIN",
                                                    13,
                                                )
            is_success = False
            for fcm_token in fcm_tokens:
                try:
                    firebase_connector.push_notification(
                        generate_payload(
                            title=formatted_title,
                            fcm_token=fcm_token,
                            message_body=formatted_message,
                            action=action,
                            order_id=order_id,
                            payload_params=payload_params,
                        )
                    )
                    is_success = True
                except (FCMTokenExpired, FirebaseError):
                    raise
            if not is_success and fcm_tokens:
                raise



PUSH_NOTIFICATIONS = {
    "group": {
        GroupStatus.EXPIRED: {
            "title": "externalNotificationGroupExpiredTitle",
            "message": "externalNotificationGroupExpiredMessage",
        },
        "five_minutes_after_creation": {
            "title": "externalNotificationAfterFiveMinutesTitle",
            "message": "externalNotificationAfterFiveMinutesMessage",
        },
        "one_hour_after_creation": {
            "title": "externalNotificationAfterOneHourTitle",
            "message": "externalNotificationAfterOneHourMessage",
        },
        "twenty_three_hours_after_creation": {
            "title": "externalNotificationAfterTwentyThreeHoursTitle",
            "message": "externalNotificationAfterTwentyThreeHoursMessage",
        },
    },
    "order": {
        OrderStatus.CONFIRMED: {
            "title": "externalNotificationOrderConfirmedTitle",
            "message": "externalNotificationOrderConfirmedMessage",
        },
        OrderStatus.READY_FOR_PICKUP: {
            "title": "externalNotificationOrderReadyForPickupTitle",
            "message": "externalNotificationOrderReadyForPickupMessage",
        },
        OrderStatus.CANCELLED: {
            "title": "externalNotificationOrderCancelledTitle",
            "message": "externalNotificationOrderCancelledMessage",
        },
        OrderStatus.COLLECTED: {
            "title": "externalNotificationOrderCollectedTitle",
            "message": "externalNotificationOrderCollectedMessage",
        },
        "ready_for_pickup_reminder": {
            "title": "externalNotificationCustomerReminderTitle",
            "message": "externalNotificationCustomerReminderMessage",
        },
        "delivery_date_update": {
            "title": "externalNotificationDeliveryDateChangeTitle",
            "message": "externalNotificationDeliveryDateChangeMessage",
        },
        "removed_items": {
            "title": "externalNotificationRemovedItemsTitle",
            "message": "externalNotificationRemovedItemsMessage",
        },
    },
    "internal": {
        OrderStatus.CANCELLED: {
            "title": "externalNotificationInternalCancelledTitle",
            "message": "externalNotificationInternalCancelledMessage",
        },
    },
    "pickup_location": {
        "reminder": {
            "title": "externalNotificationPickupLocationReminderTitle",
            "message": "externalNotificationPickupLocationReminderMessage",
        },
        OrderStatus.CONFIRMED: {
            "title": "externalNotificationPickupLocationNewOrderTitle",
            "message": "externalNotificationPickupLocationNewOrderMessage",
        },
        "created": {
            "title": "externalNotificationPickupLocationCreatedSuccessfullyTitle",
            "message": "externalNotificationPickupLocationCreatedSuccessfullyMessage",
        },
        "upcoming_orders": {
            "title": "externalNotificationPickupLocationUpcomingOrdersTitle",
            "message": "externalNotificationPickupLocationUpcomingOrdersMessage",
        },
    },
    "user": {
        "community_leader": {
            "title": "externalNotificationBecomeCommunityLeaderTitle",
            "message": "externalNotificationBecomeCommunityLeaderMessage",
        }
    },
}