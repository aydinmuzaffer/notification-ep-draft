import enum
import random
class Languages(str, enum.Enum):
    EN = "en"
    TH = "th"

class NoTranslationKeyError(ValueError):
    pass

class TranslationConnector:
        def __init__(self, url: str, timeout: int = 10):
            self.cached_translations = {}
            self.EXPIRES_AT_KEY = "expires_at"
            self.TRANSLATIONS_KEY = "translations"
            self.CACHE_TTL_MINUTES = 10
            self._url: str = url
            self._timeout: int = timeout

        def get_translation(self, language: Languages, key: str) -> str:
            rand = random.randint(3,8)

            if rand == 3:
                raise NoTranslationKeyError
            if rand == 6:
                raise KeyError
            return "this is a translation"