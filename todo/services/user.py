

from uuid import UUID
import random

class User:
    def __init__(self):
        self.fcm_tokens = [] 

class UserServiceError(Exception):
    pass
class UserNotExistError(UserServiceError):
    pass
class UserBlockedError(UserServiceError):
    pass
class UserRemovedError(UserServiceError):
    pass

class UserService:
    @staticmethod
    def get_user_by_id(
        user_id: UUID, raise_on_blocked: bool = False, raise_on_removed: bool = False, load_related: bool = False
    ) -> User:

        user = User()
        rand = random.randint(3, 6)

        if rand == 4:
            raise UserNotExistError
        if raise_on_blocked and rand == 5:
            raise UserBlockedError("User was blocked.")
        if raise_on_removed and rand == 6:
            raise UserRemovedError
        return user

      