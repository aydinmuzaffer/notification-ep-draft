from typing import Optional, TypedDict
import random

class PayloadParams(TypedDict, total=False):
    utm_campaign: str

 
class FirebaseError(Exception):
    pass


class FCMTokenExpired(FirebaseError):
    pass

def generate_payload(
    title: str,
    fcm_token: str,
    message_body: str,
    action: Optional[str] = None,
    order_id: Optional[int] = None,
    payload_params: Optional[PayloadParams] = None,
) -> dict:
    payload_params = payload_params or {}
    return {
        "validate_only": False,  # Flag for testing the request without actually delivering the message.
        "message": {
            "notification": {"title": title, "body": message_body},
            "token": fcm_token,
            "data": {
                "click_action": "FLUTTER_NOTIFICATION_CLICK",
                "action": action,
                "order_id": order_id,
                **payload_params,
            },
        },
    }

class FirebaseConnector:
        """Connects to Firebase Project"""
        def __init__(self, gcp_project_number: int, service_account_path: str, api_key: str, domain: str, timeout: int):
            self._firebase_url = f"https://fcm.googleapis.com/v1/projects/{gcp_project_number}"
            self._api_key = api_key
            self._service_account_path = service_account_path
            self._domain = domain
            self._dynamic_url = f"https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key={self._api_key}"
            self._link_analytics_url_fmt = (
                "https://firebasedynamiclinks.googleapis.com/v1/{link}/linkStats?durationDays=365"
            )
            self._credentials = None
            self._token = None
            self._timeout = timeout
        def push_notification(self, payload: dict) -> None:
            rand = random.randint(3,8)
            if rand == 3:
                raise FirebaseError
            if rand == 6:
                raise FCMTokenExpired
