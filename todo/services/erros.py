from enum import Enum, unique


class Error(Enum):
    #user
    ERROR_USER_IS_BLOCKED = "e0064", "User was blocked"
    ERROR_USER_IS_REMOVED = "e0065", "User was removed"
    ERROR_USER_NOT_EXIST= "e0800", "User does not exist"
    # notification
    NO_TRANSLATION_KEY = "e0801", "No translation key found"
    FCM_TOKEN_EXPIRED = "e0802", "FCM Token Expired"
    FIREBASE_UNAVAILABLE = "e0803", "Firebase is unavailable"
    DOMAIN_NOT_EXIST= "e0804", "Domain does not exist"
    DOMAIN_KEY_NOT_EXIST = "e0805", "Domain Key does not exists"
    
    def __init__(self, code: str, message: str) -> None:
        self.code = code
        self.message = message
