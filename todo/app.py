from genericpath import exists
from http import HTTPStatus
from typing import Dict, Optional, Union
from flask import Flask, Response, abort
from flask_restx import Api, Resource, fields
from uuid import UUID

from todo.services.notification import NotificationService, PUSH_NOTIFICATIONS
from todo.services.translation_connector import NoTranslationKeyError
from todo.services.firebase_connector import FCMTokenExpired, FirebaseError, PayloadParams
from todo.services.user import UserBlockedError, UserNotExistError, UserRemovedError
from todo.services.erros import Error

app = Flask(__name__)
api = Api(app, version='1.0', title='NotificationMVC API',
    description='A simple NotificationMVC API',
)

ns = api.namespace('notification', description='Notification operations')

payload_params = api.model('PayloadParams', {
    'utm': fields.String(required=True, description='Utm')
})

optional_params_desc = """
        For optional_params field you can set two fields named 'title_kwargs' and 'message_kwargs'. Type of this fields is dictionary. 
        In those fields you can send the message placeholders to format the title and the message of the push notfication.
        
        "optional_params": {
            "title_kwargs": {
                "name": "tuğba",
                "order_id": "13",
                "total_amount": 15
            },
            "message_kwargs": {
                "time": "13:30",
                "total_amount": 15
            },
            "action": "navigate_to_consumer_order_details",
            "order_id": "13",
            "payload_params": {
                "utm": "asdad"
            }
        }
"""
notification = api.model('Notification', {
    'user_id': fields.String(required=True, description='User Id'),
    'domain': fields.String(required=True, description='Domain'),
    'key': fields.String(required=True, description='Domain Key'),
    'optional_params':fields.Raw(required=False, description=optional_params_desc)
})

class NotificatioDAO(object):
    def __init__(self):
        self.counter = 0
        self.notifications = []

    def get(self, user_id):
        for notification in self.notifications:
            if notification['user_id'] == user_id:
                return notification
        api.abort(404, "Notification {} doesn't exist".format(user_id))

    def create(self, data):
        self.notifications.append(data)
        return data

    def update(self, user_id, data):
        notification = self.get(user_id)
        notification.update(data)
        return notification

    def delete(self, user_id):
        notification = self.get(user_id)
        self.notifications.remove(notification)    

dummy_notification = {
    "user_id": "98e78c77-32be-4165-83d3-c5e31740691d",
    "domain": "order",
    "domain_key": "COMPLETED",
    "title_kwargs": {
        "name": "tuğba",
        "order_id": 13,
        "total_amount": 15
    },
    "message_kwargs": {
        "time": "13:30",
        "total_amount": 15
    },
    "action": "navigate_to_consumer_order_details",
    "order_id": "13",
    "payload_params": {
        "utm": "asdad"
    }
}
DAO = NotificatioDAO()
DAO.create(dummy_notification)

title_kwargs = []
message_kwargs = []
payload_params = []
orders = []
actions = []

@ns.route('/')
class NotificationList(Resource):
    '''Shows a list of all notifications, and lets you POST to add new notification'''
    @ns.doc('list_notifications')
    @ns.marshal_list_with(notification)
    def get(self):
        '''List all notifications'''
        return DAO.notifications

    @ns.doc('create_notification')
    @ns.expect(notification)
    @ns.marshal_with(notification, code=201)
    def post(self):
        '''Create a new notification'''
        payload = api.payload 
        # if payload.get("title_kwargs"):
        #     title_kwargs.append(payload.get("title_kwargs"))
        # if payload.get("message_kwargs"):
        #     message_kwargs.append(payload.get("message_kwargs"))
        # if payload.get("payload_params"):  
        #     payload_params.append(payload.get("payload_params"))       
        return DAO.create(payload), 201

@ns.route('/<string:user_id>')
@ns.response(404, 'Notification not found')
@ns.param('user_id', 'The notification identifier')
class Notification(Resource):
    '''Show a single notification item and lets you delete them'''
    @ns.doc('get_notification')
    @ns.marshal_with(notification)
    def get(self, user_id):
        '''Fetch a given resource'''
        return DAO.get(user_id)

    @ns.doc('delete_notification')
    @ns.response(204, 'Notification deleted')
    def delete(self, user_id):
        '''Delete a notification given its identifier'''
        DAO.delete(user_id)
        return '', 204

    @ns.expect(notification)
    @ns.marshal_with(notification)
    def put(self, user_id):
        '''Update a notification given its identifier'''
        return DAO.update(user_id, api.payload)

@ns.route("/send")
class NotificationView(Resource):
    @staticmethod
    def err_response(http_code: int, error: Error, custom_message: Union[str, Dict] = None):
        """Return non 2xx responses with provided error message and error code and http code"""
        response = Response()
        response.headers["X-Error-Code"] = error.code
        abort(http_code, custom_message if custom_message else error.message, response=response)
    @ns.expect(notification)
    @ns.response(200, "Success")
    @ns.response(404, "Country not found")
    @ns.response(400, f"""Invalid request.\n* `{Error.DOMAIN_NOT_EXIST.code}` `{Error.DOMAIN_NOT_EXIST.message}`
                                          \n* `{Error.DOMAIN_KEY_NOT_EXIST.code}` `{Error.DOMAIN_KEY_NOT_EXIST.message}`   
                                           """)

    def post(self):
        """Sends a push notification to Firebase"""
        try:
            payload = api.payload
            domain = payload.get("domain")
            domain_key = payload.get("key")
            
            try:
                _ = PUSH_NOTIFICATIONS[domain]
            except KeyError:
                return self.err_response(HTTPStatus.BAD_REQUEST, Error.DOMAIN_NOT_EXIST)

            try:
                _ = PUSH_NOTIFICATIONS[domain][domain_key]
            except KeyError:
                return self.err_response(HTTPStatus.BAD_REQUEST, Error.DOMAIN_KEY_NOT_EXIST)

            notification_data = notification_send_payload_to_push_notifaction_dto(payload)
            NotificationService.send_push(**notification_data)

        except UserBlockedError:
            return self.err_response(HTTPStatus.NOT_FOUND, Error.ERROR_USER_IS_BLOCKED)
        except UserRemovedError:
            return self.err_response(HTTPStatus.NOT_FOUND, Error.ERROR_USER_IS_REMOVED)
        except UserNotExistError:
            return self.err_response(HTTPStatus.NOT_FOUND, Error.ERROR_USER_NOT_EXIST)
        except (NoTranslationKeyError, KeyError):
            return self.err_response(HTTPStatus.NOT_FOUND, Error.NO_TRANSLATION_KEY)
        except FCMTokenExpired:
            return self.err_response(HTTPStatus.NOT_FOUND, Error.FCM_TOKEN_EXPIRED)
        except FirebaseError:
            return self.err_response(HTTPStatus.SERVICE_UNAVAILABLE, Error.FIREBASE_UNAVAILABLE)

        return {"message": "well done"}, HTTPStatus.OK


def notification_send_payload_to_push_notifaction_dto(payload: dict) -> dict:
    optional_params =  payload.get("optional_params")  
    if not optional_params:
        optional_params = {}
    return {
        "user_id":payload.get("user_id"),
        "key":payload.get("key"),
        "domain":payload.get("domain"),
        "title_kwargs":optional_params.get("title_kwargs"),
        "message_kwargs":optional_params.get("message_kwargs"),
        "action":optional_params.get("action"),
        "order_id":optional_params.get("order_id"),
        "payload_params":optional_params.get("payload_params") 
    }  
if __name__ == '__main__':
    app.run(debug=True)